const fs = require('fs');
const { type } = require('os');

function actorNameToString(ident) {
    return `${ident.civ} ${ident.prenom} ${ident.nom}`; 
}
function writeJob(job){
    console.log(`${job.libelleCourant} - Cat ${job.socProcINSEE.famSocPro} `)
}

const organe = {
    get: async function (organeRef){
        return new Promise(async (resolve, reject) => {
            fs.readFile("./files/organe/" + organeRef + ".json", "utf-8",(err, json) => {
                if(!err){
                    const data = JSON.parse(json).acteur;
                    resolve(data)
                }
                else reject(err);
            })
        })
    }
}

const acteur = {
    getActeur: async function (acteurRef){
        return new Promise(async (resolve, reject) => {
            fs.readFile("./files/acteur/" + acteurRef + ".json", "utf-8",(err, json) => {
                if(!err){
                    const data = JSON.parse(json).acteur;
                    resolve(data)
                }
                else reject(err);
            })
        })
    },
    getActeurs: async function () {
        return new Promise(async (resolve, reject) => {
            const l = new Object();
            fs.readdir("./files/acteur", async (err, acteurs) => {
                if(!err){
                    const data = new Array();
                    for(const act of acteurs){
                        const e = await acteur.getActeur(act.replace('.json', '')).catch(() => {});
                        data.push(e);
                        // const profession = e.profession;
                        // const catProf = profession.socProcINSEE.famSocPro;
                        // if(l[catProf]){
                        //     l[catProf]++;
                        // }
                        // else{
                        //     l[catProf] = 1;
                        // }
                        console.log(actorNameToString(e.etatCivil.ident))
                        // writeJob(e.profession)
                    }
                    resolve(data);
                }
                else reject(err);
            })
        })
    }        
}


async function writeAuteur(auteur){
    if(auteur.acteur){
        acteur.getActeur(auteur.acteur.acteurRef).then(async personne => {
            console.log(`${actorNameToString(personne.etatCivil.ident)} ${auteur.acteur.qualite}`)
        }).catch(console.error)
    }
    else if(auteur.organe){
        //Faire lien avec organe
        organe.get(auteur.organe.organeRef).then(async org => {
            console.log(org)
        }).catch(console.error);
    }
    else console.error(auteur);
}

async function getDoc(){
    fs.readdir("./files/document", (err, documents) => {
        if(!err){
            for(const doc of documents){
                // console.log(doc);
                fs.readFile("./files/document/" + doc, "utf-8", async (err, json) => {
                    if(!err){
                        const data = JSON.parse(json).document;
                        // console.log(data.titres.titrePrincipal);
                        // console.log(data.classification.famille);
                        
                        const auteurs = data.auteurs.auteur;
                        // console.log(auteurs)
                        if(auteurs[0]){
                            for(const auteur of auteurs){
                                writeAuteur(auteur)
                            }
                        }
                        else{
                            writeAuteur(auteurs);
                        } 

                        await getActeur(data.auteurs.auteur.acteur.acteurRef);
                    }
                })
                
            }
        }
    })
}
// getDoc()
acteur.getActeurs()