const fs = require('fs');

fs.readFile('./aides.json', "utf-8", (err, file) => {
    if(!err){
        const data = JSON.parse(file);
        const sorted = data.sort((a, b) => {
            if(b.fields.annee != a.fields.annee) return b.fields.annee - a.fields.annee
            else return b.fields.total_des_aides_individuelles_1_2_3_en_eur - a.fields.total_des_aides_individuelles_1_2_3_en_eur
        })
        const res = new Object();
        for(const grp of sorted){
            let an = grp.fields.annee 
            if(res[an]){
                res[an].push(grp)
            }
            else{
                res[an] = [grp]
            }
        }
        for(const an in res){
            fs.writeFile("./"+an+".json", JSON.stringify(res[an]), (err) => {
                console.log(err);
                console.log(an + " saved")
            })
        }
    }
    else console.error(err);
})